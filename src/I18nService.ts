const messages = {
  hu: {
    try: 'Kipróbálom',
    helpButton: 'Segítség',
    restart: 'Újrakezdem',
    won: 'Nyertél',
    wonSteps: 'Sikerült {steps} lépésből kitalálnod a kombinációt!',
    playAgain: 'Újra játszom',
    lost: 'Veszítettél',
    lostSteps: 'Sajnos nem sikerült {steps} lépésből kitalálnod a kombinációt...',
    retry: 'Újra megpróbálom',
    help:
      'A Titkos Kód egy logikai játék. A játékot a gép ellen játszod. Célja, hogy kitaláld a {slotsCount} színből álló színkombinációt. Először tippelned kell egyet, azáltal, hogy a jobb oldalon lévő talonból korongokat teszel az aktuális sorra. A "kipróbálom" gombra kattintva a gép kiértékeli a kombinációt: a szürke jelzés azt jelenti, hogy eltaláltál egy színt, a fekete pedig azt, hogy helyes a pozíciója is. Ha sikerült legfeljebb {stepsCount} lépésből kitalálnod a kombinációt, akkor nyertél.',
    understand: 'Értem!',
  },
  en: {
    try: 'Try combination',
    helpButton: 'Help',
    restart: 'Restart game',
    won: 'You won!',
    wonSteps: 'You managed to guess the combination in {steps} steps!',
    playAgain: 'Play again',
    lost: 'You lost',
    lostSteps: "Unfortunately, you couldn't figure out the combination in {steps} steps...",
    retry: 'Retry',
    help:
      'The Secret Code is a logic game. You play the game against the machine. Your goal is to guess the color combination of {slotsCount} colors. You must first guess one by placing discs from the talon on the right on the current row. By clicking on the "Try combination" button, the machine evaluates the combination: the gray mark means that you have hit a color, and the black mark indicates that your position is also correct. If you managed to guess the combination in up to {stepsCount} steps, you won.',
    understand: 'Understand!',
  },
};

type MessageMap = { [key: string]: { [key: string]: string } };
type InterpolationMap = { [key: string]: string | number };

export class I18nService {
  messages: MessageMap;
  locale: string;

  constructor(messages: MessageMap, locale: string) {
    this.messages = messages;
    this.locale = locale;
  }

  private processMessage(rawMessage: string, interpolationMap?: InterpolationMap): string {
    if (!interpolationMap) {
      return rawMessage;
    }

    return rawMessage.replace(/{([^{}]*)}/g, (regex, key) => interpolationMap[key].toString());
  }

  get(key: string, interpolationMap?: InterpolationMap): string {
    const rawMessage = this.messages[this.locale][key] as string;
    return this.processMessage(rawMessage, interpolationMap);
  }
}

export default new I18nService(messages, 'hu');
