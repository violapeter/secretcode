import { Combination, GameBoard, Observer, Results, SLOTS_COUNT, STEPS_COUNT } from './common';
import { Game } from './Game';

type Save = {
  combination?: Combination;
  board?: GameBoard;
  results?: Results;
  step?: number;
};

export class BoardSaver implements Observer {
  private static key = 'hu.kodfejto.game';
  storage: Storage;

  constructor(storage: Storage) {
    this.storage = storage;
  }

  private getSave(): Save | null {
    const save = this.storage.getItem(BoardSaver.key);
    return save ? JSON.parse(save) : null;
  }

  private save(save: Save): void {
    this.storage.setItem(BoardSaver.key, JSON.stringify(save));
  }

  private saveGame(game: Game): void {
    const save = this.getSave();
    const { board, results, step } = game;
    this.save(save ? { ...save, board, results, step } : { board, results, step });
  }

  saveCombination(combination: Combination): void {
    const save = this.getSave();
    this.save(save ? { ...save, combination } : { combination });
  }

  getSavedCombination(): Combination | null | undefined {
    const save = this.getSave();
    return save ? save.combination : null;
  }

  getSavedGame(): Game | null {
    const save = this.getSave();
    const combination = this.getSavedCombination();
    return save && combination
      ? new Game(combination, STEPS_COUNT, SLOTS_COUNT, save.results, save.board, save.step)
      : null;
  }

  update(game: Game): void {
    this.saveGame(game);
  }
}
