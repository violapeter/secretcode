import i18n from './I18nService';
import { Game } from './Game';
import { GameState, Observer } from './common';

export class Overlay {
  content: HTMLElement | string;
  buttonLabel: string;
  callback: () => void;
  private rootElement: HTMLElement = document.createElement('div');

  constructor(content: HTMLElement | string, buttonLabel: string, callback?: () => void) {
    this.content = content;
    this.buttonLabel = buttonLabel;
    this.callback = callback || this.close;

    this.render();
    this.setListener();
  }

  private static getMarkup(buttonLabel: string): string {
    return `<div class="overlay-box">
      <div class="overlay-content"></div>
      <div class="overlay-action">
        <button class="button button-primary">${buttonLabel}</button>
      </div>
    </div>`;
  }

  private render(): void {
    this.rootElement = document.createElement('div');
    const template = Overlay.getMarkup(this.buttonLabel);
    this.rootElement.classList.add('overlay');
    this.rootElement.setAttribute('role', 'dialog');
    this.rootElement.innerHTML = template;
    const contentRoot = this.rootElement.querySelector('.overlay-content') as HTMLElement;
    if (typeof this.content === 'string') {
      contentRoot.innerHTML = this.content;
    } else {
      contentRoot.appendChild(this.content);
    }

    document.body.append(this.rootElement);
  }

  private setListener() {
    const button = this.rootElement.querySelector('.button') as HTMLElement;
    button.addEventListener('click', () => {
      this.close();
      this.callback();
    });
  }

  open(): void {
    this.rootElement.classList.add('visible');
  }

  close(): void {
    this.rootElement.classList.remove('visible');
  }
}

type Noop = () => void;

export class MessengerService implements Observer {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  replay: Noop = () => {};
  wonOverlay: Overlay = new Overlay(i18n.get('won'), i18n.get('playAgain'), () => this.replay());
  lostOverlay: Overlay = new Overlay(i18n.get('lost'), i18n.get('retry'), () => this.replay());

  constructor(replay: () => void) {
    this.replay = replay;
  }

  update(game: Game): void {
    this.wonOverlay[game.state === GameState.Won ? 'open' : 'close']();
    this.lostOverlay[game.state === GameState.Lost ? 'open' : 'close']();
  }
}
