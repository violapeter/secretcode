export enum Color {
  Grey = 'grey',
  Green = 'green',
  Blue = 'blue',
  Purple = 'purple',
  Pink = 'pink',
  Yellow = 'yellow',
  Orange = 'orange',
  Red = 'red',
}

export enum GameState {
  Won,
  InProgress,
  Lost,
}

export const SLOTS_COUNT = 4;
export const STEPS_COUNT = 10;

export type Combination = Array<Color>;
export type Result = { locationMatch: number; colorMatch: number };
export type Results = Array<Result>;
export type GameBoard = Array<Array<Color | undefined>>;

export function getRandomInt(min: number, max: number): number {
  const minInt = Math.ceil(min);
  const maxInt = Math.floor(max);
  return Math.floor(Math.random() * (maxInt - minInt + 1)) + minInt;
}

export function getRandomColor(): Color {
  const values = Object.values(Color);
  const randomIndex = getRandomInt(0, values.length - 1);
  return values[randomIndex] as Color;
}

export function isColor(value: string | undefined): boolean {
  return Object.values(Color).includes(value as Color);
}

export function isCombination(array: Array<string | undefined>): boolean {
  return Array.isArray(array) && array.length === SLOTS_COUNT && array.every(isColor);
}

export interface Subject {
  subscribe(observer: Observer): void;
  unsubscribe(observer: Observer): void;
  notify(): void;
}

export interface Observer {
  update(subject: Subject): void;
}

export abstract class GameSubject implements Subject {
  private observers: Array<Observer> = [];

  subscribe(observer: Observer): void {
    if (this.observers.includes(observer)) {
      throw new Error('Observer is already subscribed');
    }

    this.observers.push(observer);
  }

  unsubscribe(observer: Observer): void {
    const observerIndex = this.observers.indexOf(observer);

    if (observerIndex === -1) {
      throw new Error('There is no observer like this');
    }

    this.observers.splice(observerIndex, 1);
  }

  notify(): void {
    for (const observer of this.observers) {
      observer.update(this);
    }
  }
}
