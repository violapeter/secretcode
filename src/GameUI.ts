import { Game } from './Game';
import { Color, Observer, Result } from './common';
import i18n from '../src/I18nService';
import { CombinationComparator } from './CombinationComparator';

function createWithClasses(
  tagName: keyof HTMLElementTagNameMap,
  classes?: Array<string> | string,
): HTMLElement {
  const el = document.createElement(tagName);
  if (Array.isArray(classes)) {
    el.classList.add(...Array.from(classes));
  } else if (classes) {
    el.classList.add(classes as string);
  }

  return el;
}

function div(classes?: Array<string> | string): HTMLDivElement {
  return createWithClasses('div', classes) as HTMLDivElement;
}

export class GameUI implements Observer {
  update(game: Game): void {
    const gameBoardElement = this.render(game);
    const mountingElement = document.querySelector('.game') as Element;
    const previousState = mountingElement.querySelector('.game-board') as ChildNode;
    if (previousState) {
      mountingElement.removeChild(previousState);
    }
    mountingElement.append(gameBoardElement);
  }

  private render(game: Game): HTMLElement {
    const root = div('game-board');
    root.append(GameUI.renderTryButton(game));
    root.append(this.renderDots(game));
    root.append(this.renderSteps(game));
    return root;
  }

  private static renderTryButton(game: Game) {
    const classes = ['button', 'button-secondary', 'try'];

    if (!game.isCurrentStepFull) {
      classes.push('invisible');
    }

    const button = createWithClasses('button', classes) as HTMLButtonElement;
    button.innerText = i18n.get('try');
    GameUI.setButtonListener(button, game);
    return button;
  }

  private renderSteps(game: Game): HTMLElement {
    const { stepsCount, slotsCount, board, step: currentStep, results: gameResults } = game;
    const steps = div('steps');
    const slots = [];

    let i = 0;
    while (i < stepsCount) {
      const step = div('step');
      const isActiveStep = currentStep === i;

      if (isActiveStep) {
        step.classList.add('active');
      }

      const results = GameUI.renderResults(slotsCount, gameResults[i]);

      let j = 0;
      while (j < slotsCount) {
        const slot = div('slot');
        slot.dataset.color = board[i][j];
        if (isActiveStep) {
          slots.push(slot);
        }
        step.append(slot);
        j++;
      }
      step.append(results);
      steps.append(step);
      i++;
    }

    this.setSlotListeners(slots, game);

    return steps;
  }

  private static renderResults(slotsCount: number, currentResult: Result): HTMLElement {
    const results = div('results');

    let i = 0;
    while (i < slotsCount) {
      const result = div('result');
      const { locationMatch, colorMatch } = currentResult;

      if (locationMatch > i) {
        result.classList.add('location-match');
      }

      if (locationMatch + colorMatch > i) {
        result.classList.add('color-match');
      }

      results.append(result);
      i++;
    }

    return results;
  }

  private renderDots(game: Game): HTMLElement {
    const dotsParent = div('dots');
    const dots: Array<HTMLElement> = [];

    Object.values(Color).forEach((color) => {
      const classes = ['dot'];

      if (game.isColorInCurrentCombination(color)) {
        classes.push('active');
      }

      const dot = div(classes);
      dot.dataset.color = color;
      dots.push(dot);
      dotsParent.append(dot);
    });

    this.setDotListeners(dots, game);

    return dotsParent;
  }

  private static setButtonListener(button: HTMLButtonElement, game: Game) {
    button.addEventListener('click', () => game.evaluateCombination(new CombinationComparator()));
  }

  private setDotListeners(dots: Array<HTMLElement>, game: Game) {
    dots.forEach((dot) => {
      const { color } = dot.dataset;
      if (game.isColorInCurrentCombination(color as Color)) return;
      let clone: HTMLElement;
      dot.setAttribute('draggable', 'true');
      dot.addEventListener('dragstart', (event) => {
        const target = event.target as HTMLElement;
        clone = dot.cloneNode() as HTMLElement;
        clone.classList.add('ghost');
        const offset = target.offsetHeight / 2;
        event.dataTransfer?.setDragImage(clone, offset, offset);
        if (target.dataset.color) {
          event.dataTransfer?.setData('text/plain', target.dataset.color);
        }
        document.body.appendChild(clone);
      });
      dot.addEventListener('dragend', () => clone.remove());
      dot.addEventListener('click', () => game.addDot(color as Color));
    });
  }

  private setSlotListeners(slots: Array<HTMLElement>, game: Game) {
    slots.forEach((slot, index) => {
      slot.addEventListener('dragover', (event) => {
        slot.classList.add('dragover');
        event.preventDefault();
      });

      slot.addEventListener('dragenter', (event) => {
        event.preventDefault();
      });

      slot.addEventListener('dragleave', () => slot.classList.remove('dragover'));

      slot.addEventListener('drop', (event) => {
        slot.classList.remove('dragover');
        const color = event.dataTransfer?.getData('text/plain');
        game.addDot(color as Color, index);
      });

      slot.addEventListener('click', () => game.removeDot(index));
    });
  }
}
