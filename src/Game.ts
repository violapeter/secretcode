import {
  Color,
  Combination,
  GameBoard,
  GameState,
  GameSubject,
  Results,
  isCombination,
} from './common';
import { CombinationComparator } from './CombinationComparator';

export class Game extends GameSubject {
  private readonly combination: Combination;
  state: GameState;
  results: Results;
  board: GameBoard;
  step: number;
  stepsCount: number;
  slotsCount: number;

  constructor(
    combination: Combination,
    stepsCount: number,
    slotsCount: number,
    results?: Results,
    board?: GameBoard,
    step?: number,
  ) {
    super();
    this.stepsCount = stepsCount;
    this.slotsCount = slotsCount;
    this.state = GameState.InProgress;
    this.step = step || 0;
    this.combination = combination;
    this.results =
      results ||
      Array(stepsCount)
        .fill(undefined)
        .map(() => ({
          locationMatch: 0,
          colorMatch: 0,
        }));
    this.board =
      board ||
      Array(stepsCount)
        .fill(undefined)
        .map(() => Array(slotsCount).fill(undefined));
  }

  evaluateCombination(comparator: CombinationComparator): void {
    this.step += 1;
    this.results = this.results.map((result, index) => {
      const currentCombination = this.board[index];

      if (!isCombination(currentCombination)) {
        return result;
      }

      const comparedResult = comparator.compare(
        currentCombination as Combination,
        this.combination,
      );

      if (comparedResult.locationMatch === this.slotsCount) {
        this.state = GameState.Won;
      }

      if (comparedResult.locationMatch !== this.slotsCount && this.step === this.stepsCount) {
        this.state = GameState.Lost;
      }

      return comparedResult;
    });

    this.notify();
  }

  addDot(dot: Color, slot: number = this.firstEmptySlotIndex): void {
    this.board[this.step][slot] = dot;
    this.notify();
  }

  removeDot(index: number): void {
    this.board[this.step][index] = undefined;
    this.notify();
  }

  get currentCombination(): Array<Color | undefined> {
    return this.board[this.step];
  }

  get firstEmptySlotIndex(): number {
    return this.currentCombination.findIndex((slot) => slot === null);
  }

  get isCurrentStepFull(): boolean {
    return isCombination(this.currentCombination);
  }

  isColorInCurrentCombination(color: Color): boolean {
    return this.currentCombination?.includes(color);
  }
}
