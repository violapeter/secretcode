import { CombinationGenerator } from './CombinationGenerator';
import { Game } from './Game';
import { GameUI } from './GameUI';
import { MessengerService, Overlay } from './MessengerService';
import i18n from './I18nService';
import { SLOTS_COUNT, STEPS_COUNT } from './common';
import { BoardSaver } from './BoardSaver';

class App {
  private boardSaver: BoardSaver;
  readonly stepsCount: number;
  readonly slotsCount: number;

  constructor(stepsCount: number, slotsCount: number, boardSaver: BoardSaver) {
    this.boardSaver = boardSaver;
    this.stepsCount = stepsCount;
    this.slotsCount = slotsCount;
    this.initHeaderButtons();
  }

  private initHeaderButtons() {
    const { stepsCount, slotsCount } = this;
    const helpOverlay = new Overlay(
      i18n.get('help', { slotsCount, stepsCount }),
      i18n.get('understand'),
    );
    const resetButton = document.querySelector('.reset-button') as HTMLElement;
    const helpButton = document.querySelector('.help-button') as HTMLElement;
    resetButton.addEventListener('click', () => this.beginNewGame());
    helpButton.addEventListener('click', () => helpOverlay.open());
  }

  private getNewGame(): Game {
    const combination = CombinationGenerator.generate(this.slotsCount);
    boardSaver.saveCombination(combination);
    return new Game(combination, this.stepsCount, this.slotsCount);
  }

  private loadSavedGame(): Game {
    const savedGame = this.boardSaver.getSavedGame();
    if (savedGame) {
      return savedGame;
    }
    return this.getNewGame();
  }

  private startGame(game: Game): void {
    const ui = new GameUI();
    const messengerService = new MessengerService(this.beginNewGame.bind(this));
    game.subscribe(ui);
    game.subscribe(messengerService);
    game.subscribe(boardSaver);
    game.notify();
  }

  loadOrBeginGame(): void {
    this.startGame(this.loadSavedGame());
  }

  beginNewGame(): void {
    this.startGame(this.getNewGame());
  }
}

const boardSaver = new BoardSaver(localStorage);
const app = new App(STEPS_COUNT, SLOTS_COUNT, boardSaver);
app.loadOrBeginGame();
