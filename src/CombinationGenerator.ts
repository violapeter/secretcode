import { Color, Combination, getRandomColor } from './common';

export class CombinationGenerator {
  static generate(combinationLength: number): Combination {
    const colors: Array<Color> = [];

    while (colors.length < combinationLength) {
      const currentColor = getRandomColor();
      if (!colors.includes(currentColor)) {
        colors.push(currentColor);
      }
    }

    return colors;
  }
}
