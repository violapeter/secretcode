import { CombinationGenerator } from '../src/CombinationGenerator';
import { isColor } from '../src/common';

describe('CombinationGenerator', () => {
  it('should generate a proper combination', () => {
    const combination = CombinationGenerator.generate(4);
    expect(combination.every(isColor)).toBe(true);
  });
});
