import { Game } from '../src/Game';
import { Color } from '../src/common';

describe('Game', () => {
  describe('default state', () => {
    const combination = [Color.Blue, Color.Pink, Color.Yellow, Color.Orange];
    const game = new Game(combination, 10, 4);

    it('should has a right number of steps', () => {
      expect(game.board.length).toBe(10);
    });

    it('should be at step 0', () => {
      expect(game.step).toBe(0);
    });
  });

  describe('adding dot', () => {
    const combination = [Color.Blue, Color.Pink, Color.Yellow, Color.Orange];
    const game = new Game(combination, 10, 4);

    it('should has a right number of steps', () => {
      game.addDot(Color.Blue);
      game.addDot(Color.Pink, 2);
      expect(game.board[0]).toMatchObject([Color.Blue, undefined, Color.Pink, undefined]);
    });
  });

  describe('removing dot', () => {
    const combination = [Color.Red, Color.Purple, Color.Yellow, Color.Orange];
    const board = [
      [Color.Blue, Color.Pink, Color.Yellow, Color.Orange],
      [Color.Blue, Color.Orange, undefined, Color.Green],
    ];
    const game = new Game(combination, 10, 4, [], board, 1);

    it('should has a right number of steps', () => {
      game.removeDot(3);
      expect(game.currentCombination).toMatchObject([
        Color.Blue,
        Color.Orange,
        undefined,
        undefined,
      ]);
    });
  });

  describe('finding first empty slot', () => {
    const combination = [Color.Red, Color.Purple, Color.Yellow, Color.Orange];
    const board = [[Color.Blue, Color.Orange, undefined, Color.Green]];
    const game = new Game(combination, 10, 4, [], board, 0);

    it('should has a right number of steps', () => {
      expect(game.firstEmptySlotIndex).toBe(2);
    });
  });

  describe('determine wether the current step is full', () => {
    const combination = [Color.Red, Color.Purple, Color.Yellow, Color.Orange];
    const board = [[Color.Blue, Color.Orange, undefined, Color.Green]];
    const game = new Game(combination, 10, 4, [], board);

    it('should has a right number of steps', () => {
      expect(game.isCurrentStepFull).toBe(false);
    });
  });

  describe('determine wether the current step is full', () => {
    const combination = [Color.Red, Color.Purple, Color.Yellow, Color.Orange];
    const board = [[Color.Blue, Color.Orange, Color.Pink, Color.Green]];
    const game = new Game(combination, 10, 4, [], board);

    it('should be full now', () => {
      expect(game.isCurrentStepFull).toBe(true);
    });
  });
});
