import { I18nService } from '../src/I18nService';

const i18n = new I18nService(
  {
    en: {
      hello: 'Hello, {name}!',
    },
  },
  'en',
);

describe('I18nService', () => {
  it('should resolve the interpolation', () => {
    const string = i18n.get('hello', { name: 'Peter' });
    expect(string).toBe('Hello, Peter!');
  });
});
