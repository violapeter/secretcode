import { BoardSaver } from '../src/BoardSaver';
import { Color } from '../src/common';
import { Game } from '../src/Game';

const localStorageMock = {
  setItem: jest.fn(),
  getItem: jest.fn(),
};

describe('BoardSaver', () => {
  let boardSaver: BoardSaver;
  const key = 'hu.kodfejto.game';

  beforeEach(() => {
    boardSaver = new BoardSaver((localStorageMock as unknown) as Storage);
  });

  it('should save the combination', () => {
    const combination = [Color.Blue, Color.Purple, Color.Green, Color.Pink];
    boardSaver.saveCombination(combination);
    expect(localStorageMock.setItem).toBeCalledWith(
      key,
      JSON.stringify({
        combination: ['blue', 'purple', 'green', 'pink'],
      }),
    );
  });

  it('should save the game', () => {
    const combination = [Color.Blue, Color.Purple, Color.Green, Color.Pink];
    const game = new Game(combination, 10, 4, [], [], 2);
    boardSaver.update(game);
    expect(localStorageMock.setItem).toHaveBeenLastCalledWith(
      key,
      JSON.stringify({
        board: [],
        results: [],
        step: 2,
      }),
    );
  });

  it('should get the combination', () => {
    boardSaver.getSavedCombination();
    expect(localStorageMock.getItem).toHaveBeenCalledWith(key);
  });

  it('should get the saved game', () => {
    boardSaver.getSavedGame();
    expect(localStorageMock.getItem).toHaveBeenCalledWith(key);
  });
});
