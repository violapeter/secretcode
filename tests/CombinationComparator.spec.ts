import { CombinationComparator } from '../src/CombinationComparator';
import { Color } from '../src/common';

describe('CombinationComparator', () => {
  const comparator = new CombinationComparator();

  it('should be 4 location match', () => {
    const result = comparator.compare(
      [Color.Grey, Color.Green, Color.Pink, Color.Blue],
      [Color.Grey, Color.Green, Color.Pink, Color.Blue],
    );

    expect(result).toMatchObject({
      locationMatch: 4,
      colorMatch: 0,
    });
  });

  it('should be 4 color match', () => {
    const result = comparator.compare(
      [Color.Grey, Color.Green, Color.Pink, Color.Blue],
      [Color.Green, Color.Pink, Color.Blue, Color.Grey],
    );

    expect(result).toMatchObject({
      locationMatch: 0,
      colorMatch: 4,
    });
  });

  it('should be 1 location match and 2 color match', () => {
    const result = comparator.compare(
      [Color.Orange, Color.Purple, Color.Red, Color.Blue],
      [Color.Orange, Color.Red, Color.Purple, Color.Yellow],
    );

    expect(result).toMatchObject({
      locationMatch: 1,
      colorMatch: 2,
    });
  });
});
