module.exports = {
  transform: { '^.+\\.ts$': 'ts-jest' },
  testMatch: ['**/*.spec.ts'],
  moduleFileExtensions: ['ts', 'js'],
};
