# Kódfejtő játék

Egyszerű kódfejtő játék, JavaScriptben. 

### Fejlesztés

Telepítsük a szükséges csomagokat:

    $ yarn install

Indítsuk el a file watchert:

    $ yarn dev
